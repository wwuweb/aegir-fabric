Fabric==1.14.1
paramiko==2.7.1
python-jenkins==1.6.0
PyYAML==3.11
requests==2.19.1
python-ldap==3.3.1

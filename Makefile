install ?= global

ifeq ($(install), local)
	IFLAGS += --user
endif

.PHONY: install uninstall

install:
	python -m pip --verbose install $(IFLAGS) --requirement requirements.txt

uninstall:
	python -m pip --verbose uninstall --requirement requirements.txt --yes

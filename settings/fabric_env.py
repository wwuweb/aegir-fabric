#!/usr/bin/evn python
"""Configuration helper functions for fabric core env variables."""

def preprocess(data):
    """Preprocess fabric config."""

    return data

def provision(env, setting):
    """Prepare fabric config."""

    for variable in setting:
        env[variable] = setting[variable]

#!/usr/bin/env python
"""Configuration helper functions for build severs."""

from importlib import import_module

def preprocess(data):
    """Preprocess build server config."""

    for name, config in data.iteritems():
        module = import_module(config['module'])
        instance = getattr(module, config['class'])
        data[name]['instance'] = instance(
            config['uri'],
            username=config['username'],
            password=config['api_key'])

    return data

#!/usr/bin/env python
"""Helper functions for alias configuration."""

import re

def preprocess(data):
    """Preprocess alias configuration."""

    data['site']['regex'] = re.compile(data['site']['pattern'])

    return data

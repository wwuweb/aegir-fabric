#!/usr/bin/env python
"""Library for loading and updating the Aegir settings files."""

from __future__ import absolute_import

import yaml

from importlib import import_module
from os import listdir, path
from fabric.api import abort, env, warn

def load():
    """Import the aegir settings.

    Configuration resides in YAML files, located in the top-level settings/
    directory. Each YAML file is loaded, parsed into a Python dictionary, and
    stored in the defined "aegir" key of the global Fabric "env" structure.

    Script preprocessing may be performed on settings by creating a settings
    sub-module following the namespace "settings.<settings_file_name>".
    This sub-module must implement the "preprocess(data) : data" function where
    "data" is a dictionary containing the parsed YAML.

    For example, to add preprocessing to the settings file "local.yml", create a
    module file at "settings/local.py" and implement the "preprocess" function.
    Make the desired changes to the "data" dictionary and return it.

    Settings for Fabric itself are not modified by default. In order to
    provision these settings, implement the "provision(env, setting) : None"
    function. The parameter "env" is the global Fabric environment structure,
    which can be modified freely. "setting" will contain the value of the
    current setting.

    For example, this library already defines a "host" settings module to set
    the Fabric "hosts" setting. In the "provision" function, "env.hosts" is set
    to "[setting['uri']]".
    """

    env.aegir = {}
    settings_directory = path.realpath(path.dirname(__file__))
    settings_files = tuple(
        settings_file
        for settings_file in listdir(settings_directory)
        if path.isfile(path.join(settings_directory, settings_file))
        and settings_file.lower().endswith('.yml')
    )

    for settings_file in settings_files:
        settings_path = path.join(settings_directory, settings_file)
        settings_name, _ = path.splitext(settings_file)

        with open(settings_path, 'r') as stream:
            try:
                env.aegir[settings_name] = yaml.load(stream) or {}
            except:
                warn("Failed to load %s settings." % settings_name)

        stream.close()

    for settings_name in env.aegir:
        try:
            module = import_module('settings.' + settings_name)
            preprocess = getattr(module, 'preprocess')
            env.aegir[settings_name] = preprocess(env.aegir[settings_name])
        except (ImportError, AttributeError):
            pass

    for settings_name in env.aegir:
        try:
            module = import_module('settings.' + settings_name)
            provision = getattr(module, 'provision')
            provision(env, env.aegir[settings_name])
        except (ImportError, AttributeError):
            pass

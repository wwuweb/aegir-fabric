#!/usr/bin/env python
"""Configuration helper functions for Aegir host."""

def provision(env, setting):
    """Prepare host config."""

    env.hosts = [setting['uri']]

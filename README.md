# WWU Fabric Library

A small library of tasks and helper functions to streamline tasks with the Aegir
3 hosting system.

## Requirements

- Python 2.7
- pip

## Dependencies

See the contents of ``requirements.txt``.

## Installation

Install Python 2.7 using the method of choice for your system. On Linux, this
can usually be done through the package manager. More recent versions may no
longer have Python 2 available as a package, in which case manual installation
and creation of a 'python' entry in PATH will be required.

Install pip using the method of choice for your system. pip is a package manager
for Python, and is required in order to provision the dependencies of this
  library.

You may also need to install additional dependencies so that required packages
can be built. For example, on Ubuntu, you will need to run the following
command:

```sh
$ sudo apt-get install python2-dev libldap2-dev libsasl2-dev
```

Finally, before running any tasks for the first time, use ``pip`` to install all
dependencies.

```sh
$ pip --verbose install --user --requirement requirements.txt
```

This can also be streamlined by running make. The included Makefile assumes that
a python executable is available on PATH; if this is not the case, either edit
the Makefile to point to the correct executable, or make appropriate changes to
PATH.

```sh
$ make install=local
```

Installation can be local or global (change "local" above for system-wide
installation of Fabric). If the install is local, the ``fab`` executable will be
located at ``~/.local/bin/fab``.

### Installation on Ubuntu 20.04 - 22.04

Python2 pip is not available from the package manger. Install pip2 with the
get-pip.py script:

```sh
$ curl https://bootstrap.pypa.io/get-pip.py --output get-pip.py
$ sudo python get-pip.py
```

## Usage

Fabric tasks are run via the commandline ``fab`` executable. To run a task, type
the ``fab`` command, follow with a space, an then the full name of the task.

Note that angle brackets (e.g. "<" and ">") are used to indicate placeholders in the following
examples; these brackets should not appear in the literal command.

```sh
$ fab <taskname>
```

To list the available Fabric tasks, use the command:

```sh
$ fab --list
```

This will print out a list of all available tasks, along with a brief
description explaining what the task does. Many tasks have a more detailed
description located in the source code file for that task.

Running multiple tasks per fab invokation is possible by simply listing them in
order, separated by spaces.

```sh
$ fab <task1> <task2> ... <taskn>
```

Tasks will run sequentially in the order that they are listed in the command
invokation. This means that if task A requires something from task B, task B
must be entered first in the command, before task A.

Arguments are passed to a fab task as a comma-separated list after a colon:

```sh
$ fab <task>:<arg1>,<arg2>,...,<argn>
```

The colon is not used if no arguments are passed to the task.

Keyword arguments are passed as normal in Python:

```sh
$ fab <task>:arg1=val1,arg2=val2,...,argn=valn
```

## Structure

By default, Fabric looks for a "fabfile" in the current working directory. This
is either a single file named ``fabfile.py`` or a Python module named
``fabfile``. This library uses the latter; the fabfile directory in the root of
the project contains the top-level module under which the rest of the library
and the tasks defined therein are organized.

#### settings

This module, found in the settings directory in the project root, automatically
provides custom settings in the form of YAML config files. By default, all YAML
files defined in this directory will be loaded into the env.aegir global
dictionary, keyed by the name of the file.

In addition, the settings loader will look for a module of the same name as the
YAML file, containing a "preprocess" or "provision" function. These functions
allow the loaded settings to be modified or inserted into the top-level env
dictionary (instead of env.aegir) respectively.

Settings are loaded when the ``fabfile`` module is loaded, which occurs once per
``fab`` invocation.

#### alias

Helper functions for working with drush aliases, including functions to load
full aliases from a remote Aeigr host. Other functions asist with generating
aliases in the same convention as the Aegir hosting component.

#### build_server

Provides a task for executing an arbitrary job on a remote CI (build) server.
The current implementation supports Jenkins via the jenkins Python library. This
module is required by some tasks to handle automation of bulk operations, such
as site migrations.

As it is assumed that this library will be used on a CI server, it is considered
reasonable to execute builds from within Fabric itself.

#### drush

Drush helper functions. This includes a helper function for running drush
commands locally or on the remote server.

#### exceptions

Defines custom exceptions for the propogation of errors within the library.

#### wwu

Defines more complex workflows specific to Drupal as hosted at Western
Washington University, which may not be all environments.

### Settings

Configuration is stored in YAML files for easy reading and editing. The settings
stored in these files cover the servers, platforms, and sites in Aegir, as well
as the configuration needed to define and connect to the remote host where Aegir
is running.

#### host

Configuration for the remote host.

- Name of the environment (e.g. dev, test, prod)
- URI of the host
- Aegir root directory

#### build_servers

Configuration for remote CI servers provided to the build server module. At
least one valid server instance is required by some tasks.

Build server configuration requires the implementation of the
build_server.Instance class. This class will serve as a wrapper around
the server-specific implementation used to interface with that server. For
example, Jenkins integration is built into this library with the Python Jenkins
package, using the jenkins.Jenkins class inside of the JenkinsInstance wrapper.

Entries in this config are keyed by the server instance name. This is used to
identify different configurations only; it does not need to match the canonical
name of any server.

- module: The name of the module where the Instance wrapper class is located
- class: The name of the Instance wrapper class implementation; e.g.
  JenkinsInstance
- uri: The address of the build server
- username: The username of an authorized account under which to execute jobs
- api_key: The password associated with the given username
- jobs: A mapping to server job names. This can be used to map standardized job
  names to their actual names on the build server.

```yaml
server_name:
  module: 'module_name'
  class: 'ServerInstance'
  uri: 'https://fqdn.domain.com'
  username: 'username'
  api_key: 'password'
  jobs:
    job_key_1: 'jenkins-server-job-name-1'
    job_key_2: 'jenkins-server-job-name-2'
```

## Development

### Python

[Python Documentation](https://docs.python.org/2)

Some familiarity with Python, basic scripting, and software development patterns
are required to work effectively with this codebase.

Please observe best practices when coding. Use an editor that automatically
formats your code according to the PEP8 style guide. Avoid leaving untested
code at the head of the master branch. Use Pylint to detect basic errors early
and keep code streamlined.

### Fabric

[Fabric Documentation](http://docs.fabfile.org/en)

Fabric tasks are defined like normal Python functions, with the addition of the
``@task`` decorater. Tasks can contain any valid Python code; they may invoke
other Fabric tasks or Python functions.

Any function defined as a task is then available on the commandline via ``fab``,
as documented above under Usage.

### Organization

The library code is organized into sub-modules to provide basic separation of
concerns. Wherever appropriate, creating new modules to encapsulate new tasks is
encouraged, as the resulting namespacing helps keep tasks oganized and avoids
naming collisions.

### Defining New Tasks

Most new tasks should be added under the wwu/d10 module (subject to change).
This module is loosely organized to correspond to the command categories defined
by the latest version of Drush; other modules have been created to organize
separate functionality as needed.

Task functions should be invoked using the Fabric API call ``execute()``,
passing the task name as a string literal. Avoid invoking tasks directly as a
plain function call, as this causes the code to execute outside the Fabric
execution context.

There is no restriction on creating non-task functions or calling them from
within tasks. This is encouraged to keep code organized.

Clearly document each job; PEP8 requires a brief, one-line summary at the start
of the doc block, followed by a more complete and detailed explanation. In
particular, note any workarounds or non-standard procedures, edge cases, and
unexpected behavior.

#!/usr/bin/env python
"""Alias test suite."""

import os
import unittest
import yaml

from fabfile import alias

class TestAlias(unittest.TestCase):
    """Alias test case."""

    def test_filter_record(self):
        """Test filter record."""

        valid_filtered_record = {
            'context_type': 'site',
            'platform': '@platform_prodcds7x710',
            'server': '@server_master',
            'db_server': '@server_cmsdbctswwuedu',
            'uri': 'atus7x710.wwu.edu',
            'root': '/var/aegir/platforms/prod-cds-7.x-7.1.0',
            'site_path':
            '/var/aegir/platforms/prod-cds-7.x-7.1.0/sites/atus7x710.wwu.edu',
            'site_enabled': True,
            'language': 'en',
            'client_name': 'admin',
            'aliases': ['atus.wwu.edu'],
            'cron_key': '',
            'drush_aliases': {},
            'profile': 'collegesites'
        }

        data_directory = os.path.join(os.path.dirname(__file__), 'data')
        alias_record_file = os.path.join(data_directory, 'alias_test.yml')

        with open(alias_record_file, 'r') as stream:
            site_records = yaml.load(stream) or {}

        stream.close()

        for site_alias in site_records:
            filtered_record = alias.filter_record(site_records[site_alias])
            self.assertItemsEqual(valid_filtered_record, filtered_record)

    def test_serialize_record(self):
        """Test serialize record."""

        valid_serialized_record = {
            'context_type': 'site',
            'platform': '@platform_prodcds7x710',
            'server': '@server_master',
            'db_server': '@server_cmsdbctswwuedu',
            'uri': 'atus7x710.wwu.edu',
            'root': '/var/aegir/platforms/prod-cds-7.x-7.1.0',
            'site_path':
            '/var/aegir/platforms/prod-cds-7.x-7.1.0/sites/atus7x710.wwu.edu',
            'site_enabled': True,
            'language': 'en',
            'client_name': 'admin',
            'aliases': 'atus.wwu.edu',
            'redirection': False,
            'cron_key': '',
            'drush_aliases': '',
            'profile': 'collegesites'
        }

        data_directory = os.path.join(os.path.dirname(__file__), 'data')
        alias_record_file = os.path.join(data_directory, 'alias_test.yml')

        with open(alias_record_file, 'r') as stream:
            site_records = yaml.load(stream) or {}

        stream.close()

        for site_alias in site_records:
            serialized_record = \
                alias.serialize_record(site_records[site_alias])
            self.assertEqual(valid_serialized_record, serialized_record)

    def test_format_record(self):
        """Test format record."""

        valid_formatted_record = [
            '--context_type=site',
            '--platform=@platform_prodcds7x710',
            '--server=@server_master',
            '--db_server=@server_cmsdbctswwuedu',
            '--uri=atus7x710.wwu.edu',
            '--root=/var/aegir/platforms/prod-cds-7.x-7.1.0',
            '--site_path=/var/aegir/platforms/prod-cds-7.x-7.1.0/sites/atus7x710.wwu.edu',
            '--site_enabled',
            '--language=en',
            '--client_name=admin',
            '--aliases=[\'atus.wwu.edu\']',
            '--profile=collegesites'
        ]

        data_directory = os.path.join(os.path.dirname(__file__), 'data')
        alias_record_file = os.path.join(data_directory, 'alias_test.yml')

        with open(alias_record_file, 'r') as stream:
            site_records = yaml.load(stream) or {}

        stream.close()

        for site_alias in site_records:
            formatted_record = alias.format_record(site_records[site_alias])
            self.assertItemsEqual(valid_formatted_record, formatted_record)

if __name__ == '__main__':
    unittest.main()

#!/usr/bin/env python
"""Alias test suite."""

import os
import unittest

from fabfile import alias
from fabric.api import env, path
from settings import load
from subprocess import call

class TestAlias(unittest.TestCase):
    """Alias test case."""

    def setUp(self):
        """Alias test setup."""

        working_dir = os.path.dirname(__file__)

        call(['composer', '--working-dir=%s' % working_dir, 'install'])
        load()

        env.host_string = env.aegir['host']['uri']

    def tearDown(self):
        """Alias test teardown."""

        vendor_directory = os.path.join(os.path.dirname(__file__), 'vendor')

        call('rm', '-r', vendor_directory)

    def test_exists(self):
        """Test exists."""

        vendor_bin_directory = os.path.join(
            os.path.dirname(__file__), 'vendor', 'bin')

        with path(vendor_bin_directory):
            self.assertTrue(alias.exists('@atus.prod', local=True))

if __name__ == '__main__':
    unittest.main()

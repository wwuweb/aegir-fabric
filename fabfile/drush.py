"""Functions for interfacing with Drush"""

import re
import string
import yaml

from fabric.api import abort, hide, local, sudo
from io import StringIO

def run(*args, **options):
    """
    Call drush as the sudo user on the remote host with the given arguments.

    Executes drush on the remote host with the given arguments. Arguments must
    be correct and in the appropriate order for any command that is called.
    """

    drush_args = ['drush --yes'] + list(args)

    # Fix for audiofield module.
    if 'yaml' in drush_args:
        drush_args.append("2> >(grep -v 'audiowaveform' >&2)")

    command = ' '.join(str(arg).strip() for arg in drush_args)

    with hide('warnings'):
        if 'local' in options and options['local']:
            output = local(command, capture=True)
        else:
            output = sudo(command, combine_stderr=False)

    # If YAML is requested, load and return it.
    if output.succeeded and re.search('--format(\s|=)yaml', command):
        # Set the output to the stdout string.
        output = output.stdout

        # Ensure output is Unicode for safe YAML parsing.
        if not isinstance(output, unicode):
            output = output.decode('utf-8', 'ignore')

        # Filter out non-printable characters for safe YAML parsing.
        buffer = StringIO()
        printable = set(string.printable)

        for char in output:
            if char in printable:
                buffer.write(char)

        buffer.seek(0)

        # Load the YAML.
        try:
            output = yaml.safe_load(buffer)
        except yaml.YAMLError as e:
            abort("Failed to load YAML:\n%s" % str(e))
        finally:
            buffer.close()

    return output

"""Helper functions for working with drush aliases"""

import re

from fabric.api import hide

from . import drush
from .exceptions import AliasNotFoundException

class AliasFormatDefinition(object):
    """The definition for an alias format."""

    def __init__(self, template, regex, decorator):
        self.__template = template
        self.__regex = regex
        self.__decorator = decorator

    def compose(self, components):
        """Compose an alias from the provided dictionary."""

        return self.__decorator(self.__template.format(**components))

    def decompose(self, alias):
        """Decompose the provided alias via the definition regular expression."""

        return self.__regex.search(alias).groupdict()

def _prefix_alias(alias):
    """Prefix an alias with the ampersand character."""

    prefixed_alias = alias

    if not prefixed_alias.startswith('@'):
        prefixed_alias = '@' + prefixed_alias

    return prefixed_alias

def _prefix_record_keys(records):
    """Prefix alias record keys with the ampersand character.

    Drush does not output the alias key with the ampersand available, which
    impedes indexing into an dictionary of alias records with the same alias
    string used to provide context for a drush command.

    This function iterates over the dictionary and converts all keys to have the
    ampersand.
    """

    return {
        _prefix_alias(alias): records[alias]
        for alias in records
    }

def _serialize_record_value(value):
    """Prepare an alias record value for passing to provision-save.

    The drush command provision-save only accepts comma-separated lists. List
    and dict data structures must be converted to strings before they are saved.
    """

    if type(value) is dict:
        value = value.values()

    if type(value) is list:
        value = ','.join(value)

    return value

def filter_record(record):
    """Remove alias record values that should not be re-saved to the alias.

    Calls to the drush command provision-save must not write literal booleans,
    or alias metadata (those values preceeded with the # character).
    """

    return {
        key: value
        for key, value in record.iteritems()
        if not (value is False or key.startswith('#'))
    }

def serialize_record(record):
    """Convert list and dictionary keys to a delimited string.

    Calls to the drush command provision-save must pass multi-values as comma
    separated lists.
    """

    return {
        key: _serialize_record_value(value)
        for key, value in record.iteritems()
    }

def format_record(record):
    """Format a record into the key-value flags used with drush provision-save.

    The drush provision-save command requires record keys to be passed as flags.
    Boolean values require that the flag be set if true, and absent otherwise.
    All other values are passed along with the flag if not empty.
    """

    formatted_record = []

    for key, value in record.iteritems():
        if value is True:
            formatted_record.append("--%s" % key)
        elif value:
            formatted_record.append("--%s=%s" % (key, value))

    return formatted_record

def exists(context, local=False):
    """Check if the given alias is defined on the remote server.

    Returns true if the alias is found, false otherwise.
    """

    return context in set(get_all_aliases(local))

def get_all_aliases(local=False):
    """Load all aliases without associated record data.

    Conserves bandwidth when communicating with the remote server.
    """

    aliases = []

    with hide('everything'):
        output = drush.run('site-alias', local=local)

    if output.strip():
        aliases = output.splitlines()

    return tuple(aliases)

def get_record(context):
    """Get a single record."""

    return get_records(context).get(context)

def get_records(context=None, context_type=None, local=False):
    """Load drush aliases into a dictionary.

    The alias parameter must be a fully-formed, valid alias, including the
    preceeding ampersand.
    """

    alias_records = {}

    with hide('everything'):
        args = ['site-alias']

        if context:
            args.append(context)

        args.append('--format=yaml')
        output = drush.run(*args, local=local)

    if context_type:
        alias_records = {
            alias: record
            for alias, record in alias_records.iteritems()
            if 'context_type' in record and record['context_type'] == context_type
        }

    return _prefix_record_keys(alias_records)

def get_records_by_platform(context):
    """Load all site alias records for the given platform."""

    return {
      alias: record
      for alias, record in get_records().iteritems()
      if 'platform' in record and record['platform'] == context
    }

def machine_name(string):
    """Get the macine name of the given context.

    By convention, machine names contain only letters, numbers, and underscores.
    All other symbols are stripped out. Consecutive underscores are collapsed
    into a single occurence.
    """

    converted_string = re.sub('[^a-zA-Z0-9_]', '', string)
    converted_string = re.sub('_+', '_', converted_string)

    return converted_string

def platform(platform_name):
    """Get the platform alias for the given token.

    By convention, platform aliases are the machine name of the platform
    prepended by "@platform_".
    """

    return _prefix_alias('platform_%s' % machine_name(platform_name))

def server(server_name):
    """Get the server alias for the given token.

    By convention, server aliases are the machine name of the server prepended
    by "@server_".
    """

    return _prefix_alias('server_%s' % machine_name(server_name))

def site(site_name):
    """Get the site alias for the given token.

    Since site aliases are formed from the URI of a site, this simply invovles
    prepending the given token with an ampersand.
    """

    return _prefix_alias(site_name)

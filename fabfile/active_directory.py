"""ldap functions"""

from fabric.api import env

def binddn():
    """Get the full binddn."""

    binddn = env.aegir['ldap']['binddn']
    basedn = env.aegir['ldap']['basedn']

    return "%s,%s" % (binddn, basedn)

def groupdn(group):
    """Get a full group filter from the Active Directory group name."""

    basedn = env.aegir['ldap']['basedn']

    return "cn=%s,ou=Groups,%s" % (group, basedn)

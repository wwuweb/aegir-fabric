"""Defined exceptions for Aegir library"""

class BuildServerException(Exception):
    """Wrapper exception thrown when a build server job fails."""

    pass

class AliasNotFoundException(Exception):
    """Wrapper exception thrown when an alias does not exist."""

    pass

"""Tasks for the CAS module"""

import io

from fabric.api import task

from . import drush

@task
def user_create_d10(site_alias, username, email, roles):
    """Create a new CAS user with the given username, email, and roles.

    Roles should be a comma-seperated list of existing roles to add.
    """

    return drush.run(site_alias, 'cas:user-create', username, email, roles)

@task
def user_create_d8_batch(site_alias, usernames, role):
    """Create new CAS users in bulk."""

    with io.StringIO(usernames.decode('utf-8')) as f:
        for username in f:
            username = username.rstrip('\n')
            user_create_d8(site_alias, username, "%s@wwu.edu" % username, role)

@task
def user_create_d8(site_alias, username, email, roles):
    """Create a new CAS user with the given username, email, and roles.

    Roles should be a comma-seperated list of existing roles to add.
    """

    return drush.run(site_alias, 'cas-user-create', username, email, roles)

@task
def user_create(site_alias, username):
    """Create a new CAS user with the given name."""

    return drush.run(site_alias, 'cas-user-create', username)

@task
def user_add_role(site_alias, role, user):
    """Add a role to the given CAS users."""

    args = [site_alias, 'cas-user-add-role']

    args.append("\"%s\"" % role)
    args.append(user)

    return drush.run(*args)

"""Config Update functions"""

from . import drush

def config_import(site_alias, config):
    """Import the given config for the given site."""

    return drush.run(site_alias, 'config-import-missing', config)

def config_revert(site_alias, config):
    """Revert the given config for the given site."""

    return drush.run(site_alias, 'config-revert', config)

#!/usr/bin/env python
"""Execution of library tasks via Jenkins"""

from fabric.api import abort, env, task

from ..exceptions import BuildServerException

class Instance(object):
    """Build server instance base class.

    This class acts as a wrapper around server-specific library classes or
    methods that may be used to interface with arbitrary build server endpoints.
    """

    def build_job(self, job, parameters=None, token=None):
        """Build a job method stub.

        This method should take the job name as a string and pass it to the
        wrapped build server instance method.
        """

        pass

    def instance(self):
        """Get the wrapped instance object."""

        return self.instance

@task
def build_job(job, parameters=None):
    """Build a job on a continuous integration server."""

    try:
        build_servers = env.aegir['build_servers']
    except KeyError:
        abort('Build server configuration is not loaded.')

    for server_name in build_servers:
        instance = build_servers[server_name]['instance']
        jobs = build_servers[server_name]['jobs']

        if job in jobs:
            try:
                instance.build_job(jobs[job], parameters)
            except BuildServerException as exception:
                abort('Job %s failed due to: %s' % (jobs[job], str(exception)))

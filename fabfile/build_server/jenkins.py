#!/usr/bin/env python
"""JenkinsInstance class module.

A build_server.JenkinsInstance provides a wrapper for Jenkins server instances.
"""

from __future__ import absolute_import

from jenkins import Jenkins
from jenkins import JenkinsException

from . import Instance
from ..exceptions import BuildServerException

class JenkinsInstance(Instance):
    """Build server instance class."""

    def __init__(self, uri, username=None, password=None):
        self.instance = Jenkins(uri, username=username, password=password)

    def build_job(self, job, parameters=None, token=None):
        """Build a job.

        This method should take the job name as a string and pass it to the
        wrapped build server instance method.
        """

        try:
            self.instance.build_job(job, parameters, token)
        except JenkinsException as exception:
            raise BuildServerException(exception)

"""Maintenance mode tasks."""

from fabric.api import task, hide, abort

from ... import drush
from . import alias

@task
def set_all(value):
    """Set maintenance mode state for all sites."""

    site_records = alias.load_all()

    for site_alias in site_records:
        drush.run(site_alias, 'maint:set', value, '--input-format', 'integer')

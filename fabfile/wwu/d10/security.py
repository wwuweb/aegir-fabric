"""Security tasks."""

from fabric.api import sudo, task

from . import alias

@task
def set_random_password_user_1():
    """Set a random password for user 1 on all sites."""

    site_records = alias.load_all()

    for site_alias in site_records:
        password_command = 'drush' \
            + ' %s ' % site_alias \
            + 'user-password admin --password=' \
            + '"$(' \
            + 'LC_ALL=C tr --delete --complement' \
            + " 'A-Za-z0-9 !\"#$%&'\\''()*+,-./:;<=>?@[\\\\\\]^_`{|}~' " \
            + '< /dev/urandom | head -c 128' \
            + ')"'

        sudo(password_command)

"""Core requirements tasks"""

from fabric.api import execute, hide, puts, task

from ... import drush
from . import alias

@task
def deprecated_modules(site_alias):
    """List installed deprecated modules."""

    with hide('output'):
        output = drush.run(site_alias, 'core:requirements', '--format', 'yaml')

    if (output['deprecated_modules']):
        puts(output['deprecated_modules']['title'])
        puts(output['deprecated_modules']['value'])

@task
def deprecated_modules_all():
    """List installed deprecated modules across all sites."""

    site_records = alias.load_all()

    for site_alias in site_records:
        with hide('running'):
            puts("Checking deprecated modules for %s..." % site_records[site_alias]['uri'])
            execute('build_server.build_job', 'deprecated-modules', {
                'site_alias': site_alias
            })

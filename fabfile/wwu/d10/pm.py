"""Package manager tasks."""

from fabric.api import abort, execute, hide, puts, task, warn

from . import alias
from ... import drush

def package_info(site_alias, package):
    """Get info on the given package."""

    with hide('everything'):
        output = drush.run(
            site_alias, 'pm:list', '--format', 'yaml', '--filter', package
        )

    return output

def package_enabled(site_alias, package):
    """Determine whether the given package is enabled on the given site."""

    try:
        info = package_info(site_alias, package)
        status = info[package]['status']
        puts(site_alias)
        puts("%s: %s" % (package, status), show_prefix=False, flush=True)
    except Exception as exception:
        warn(str(exception))

@task
def package_enabled_all(package):
    """Determine whether the given package is enabled on all sites."""

    site_records = alias.load_all()

    for site_alias in site_records:
        package_enabled(site_alias, package)

@task
def install_all(package):
    """Enable the given package on all sites."""

    site_records = alias.load_all()

    for site_alias in site_records:
        drush.run(site_alias,'pm:install', package)

@task
def uninstall_all(package):
    """Uninstall the given package on all sites."""

    site_records = alias.load_all()

    for site_alias in site_records:
        drush.run(site_alias, 'pm:uninstall', package)

"""Filesystem management tasks."""

from fabric.api import abort, cd, execute, hide, puts, sudo, task

from ... import drush
from . import alias

@task
def fix_permissions(site_alias):
    """Fix file permissions for the given site."""

    with hide('output'):
        output = drush.run(site_alias, 'core:status', '--format', 'yaml')

    path = output['root'] + '/' + output['site']

    with cd(path):
        puts('Set ownership and permissions of site directory:')
        sudo("chgrp -c aegir '%s'" % path)
        sudo("chmod -c 00555 '%s'" % path)

        puts('...')
        puts('Set ownership and permissions for all locations that should not be # writable by www-data:')
        sudo("find . -not -name files -not -path '*/files/*' -not -name private -not -path '*/private/*' -not -name settings.php -not -name local.settings.php -exec chgrp -c aegir '{}' \;")
        sudo("find . -type d -not -path . -not -name files -not -path '*/files/*' -not -name private -not -path '*/private/*' -exec chmod -c 00775 '{}' \;")
        sudo("find . -type f -not -path '*/files/*' -not -path '*/private/*' -not -name settings.php -not -name local.settings.php -not -name services.yml -not -name .htaccess -exec chmod -c 0664 '{}' \;")

        puts('...')
        puts('Set ownership and permissions of "files" and "private/files":')
        sudo("find . -type d -name files -exec chgrp -c www-data '{}' \;")
        sudo("find . -type d -name files -exec chmod -c 2775 '{}' \;")

        puts('...')
        puts('Set ownership of "*/files/*":')
        sudo("find . -type d -path '*/files/*' -not -user www-data -exec chgrp -c www-data '{}' \;")
        sudo("find . -type f -path '*/files/*' -not -user www-data -not -name .htaccess -exec chgrp -c www-data '{}' \;")

        puts('...')
        puts('Set permissions of "*/files/*":')
        sudo("find . -type d -path '*/files/*' -not -user www-data -exec chmod -c 2775 '{}' \;")
        sudo("find . -type f -path '*/files/*' -not -user www-data -not -name .htaccess -exec chmod -c 0664 '{}' \;")

        puts('...')
        puts('Set ownership for sensitive files:')
        sudo("find . -type f -name settings.php -exec chgrp -c www-data '{}' \;")
        sudo("find . -type f -name local.settings.php -exec chgrp -c www-data '{}' \;")
        sudo("find . -type f -name services.yml -exec chgrp -c www-data '{}' \;")

        puts('...')
        puts('Set permissiosns for sensitive files:')
        sudo("find . -type f -name settings.php -exec chmod -c 0440 '{}' \;")
        sudo("find . -type f -name local.settings.php -exec chmod -c 0440 '{}' \;")
        sudo("find . -type f -name services.yml -exec chmod -c 0440 '{}' \;")

        puts('...')
        puts('Set ownership and permissions for all ".htaccess" files:')
        sudo("find . -type f -not -user www-data -name .htaccess -exec chgrp -c www-data '{}' \;")
        sudo("find . -type f -not -user www-data -name .htaccess -exec chmod -c 0444 '{}' \;")

@task
def fix_permissions_all():
    """Fix file permissions for all sites."""

    site_records = alias.load_all()

    for site_alias in site_records:
        with hide('running'):
            puts("Fixing file permissions for %s..." % site_records[site_alias]['uri'])
            execute('build_server.build_job', 'fix-permissions', {
                'site_alias': site_alias
            })

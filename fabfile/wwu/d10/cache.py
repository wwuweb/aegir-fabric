"""Cache management tasks."""

from fabric.api import abort, execute, hide, task

from ... import drush
from . import alias

@task
def rebuild(site_alias):
    """Rebuild the cache for the given site."""

    drush.run(site_alias, 'cache:rebuild')

@task
def rebuild_all():
    """Rebuild the cache for all sites."""

    site_records = alias.load_all()

    for site_alias in site_records:
        with hide ('running'):
            execute('build_server.build_job', 'cache-rebuild', {
                'site_alias': site_alias
            })

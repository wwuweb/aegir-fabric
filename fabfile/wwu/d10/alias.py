"""Drush alias library."""

import yaml

from fabric.api import abort, env, hide

from ... import drush

def load(site_alias):
    """Load the given alias."""

    with hide('output'):
        output = drush.run('site:alias', site_alias)

    if output.failed:
        abort("Failed to fetch site alias:\n%s" % str(output))

    try:
        site_record = yaml.safe_load(output)
    except yaml.YAMLError as e:
        abort("Failed to load YAML:\n%s" % str(e))

    return site_record

def load_all():
    """Load all drush aliases."""

    with hide('output'):
        output = drush.run('site:alias', '--filter', env.aegir['host']['environment'])

    if output.failed:
        abort("Failed to fetch site aliases:\n%s" % str(output))

    try:
        site_records = yaml.safe_load(output)
    except yaml.YAMLError as e:
        abort("Failed to load YAML:\n%s" % str(e))

    return site_records

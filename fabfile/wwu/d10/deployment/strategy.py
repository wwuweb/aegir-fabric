"""Strategy library."""

from .plugin import PluginDiscovery

class strategies:
    """
    Decorator for specifying required strategy plugins.
    """

    def __init__(self, *strategy_names):
        self.strategy_names = strategy_names

    def __call__(self, cls):
        cls.required_strategies = self.strategy_names
        return cls

class Strategy:
    """
    Strategy plugin base class.
    """

class StrategyDiscovery(PluginDiscovery):
    """
    Discovers strategy plugins.
    """

    plugin_type = "strategy"

"""Deployment library."""

import os

from .artifact import ArtifactFactoryRegistry, ArtifactFactoryDiscovery
from .strategy import StrategyDiscovery

class Deployment:
    """
    Main deployment API.
    """

    def __init__(self):
        # Instantiate a new artifact factory registry to store artifact factory
        # classes, their dependencies, and their supported artifact types.
        self.registry = ArtifactFactoryRegistry()

        # Instantiate plugin discovery classes.
        artifact_factory_discovery =  ArtifactFactoryDiscovery()
        strategy_discovery = StrategyDiscovery();

        # Discover artifact factories and supported strategies.
        artifact_factories = artifact_factory_discovery.discover()
        strategies = strategy_discovery.discover()

        # Register all artifact factories and supported strategies.
        self.registry.register_factories(artifact_factories, strategies)

    def create_deployment(self, artifact_type, *args, **kwargs):
        """
        Create an artifact instance for deployment.

        Args:
            artifact_type (string): A string representing the artifact type.
            *args, **kwargs: Required arguments for the given artifact type.
        """

        # Get the artifact from the registry. The registry knows how to create
        # artifacts based on the artifact factories registered in the
        # constructor of Deployment.
        return self.registry.create_deployment(artifact_type, *args, **kwargs)

"""Template library."""

import textwrap

class TemplateFromString:
    """
    Defines a template built from a Python format string.

    A format string has named placeholders wrapped in curly braces that are
    populated by calling the "format" string method.

    The render method accepts a dictionary-like object that defines the
    parameters expected by the template. These are expanded into keyword
    arguments and passed to the string format method.
    """

    def __init__(self, string):
        self.string = string

    def render(self, parameters):
        return self.string.format(**parameters)

class TemplateFromFile:
    """
    Defines a template built from a file.

    The file must contain a valid Python format string.

    See: TemplateFromString().
    """

    def __init__(self, file):
        self.file = file

    def render(self, parameters):
        return TemplateFromString(self.file.read()).render(parameters)

class TemplateFromFunction:
    """
    Defines a template built from a callable.

    The callable must accept a dictionary of parameters that correspond to the
    placeholders defined in the template, as with all other templates defined
    here.
    """

    def __init__(self, func):
        self.func = func

    def render(self, parameters):
        return self.func(**parameters)

class DedentedTemplate:
    """
    Defines a template decorator that dedents the given template after
    rendering.
    """

    def __init__(self, template):
        self.template = template

    def render(self, parameters):
        return textwrap.dedent(self.template.render(parameters))

class TrimmedTemplate:
    """
    Defines a template decorator that trims the given template after rendering.
    """

    def __init__(self, template):
        self.template = template

    def render(self, parameters):
        return self.template.render(parameters).strip()

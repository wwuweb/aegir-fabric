"""Artifact management library."""

import inspect

from .plugin import PluginDiscovery

class artifact_type:
    """
    Decorator to mark a method as a factory for a specific artifact type.
    """

    def __init__(self, artifact_type):
        self.artifact_type = artifact_type

    def __call__(self, func):
        func.artifact_type = self.artifact_type
        return func

class Artifact:
    """
    Artifact base class.
    """

class ArtifactFactory:
    """
    Artifact Factory plugin base class.
    """

class ArtifactFactoryDiscovery(PluginDiscovery):
    """
    Discovers artifact factory plugins.
    """

    plugin_type = "artifact_factory"

class ArtifactFactoryRegistry:
    """
    Registry of supported artifact types.

    Registry entries consist of a 3-tuple holding the factory class needed to
    generate the artifact type, the strategies required by the factory to deploy
    the artifact type, and the name of the factory method used for
    instantiation of artifacts.

    Entries are stored in a dictionary keyed by the artifact type string.
    """

    def  __init__(self):
        # Registry to store supported artifact types and the data needed to
        # instantiate them.
        self.registry = {}

    def register_artifact_type(self, artifact_type, artifact_factory_func):
        """
        Register an artifact factory function. This method should normally be called from
        the register() method on an artifact factory.

        Args:
            artifact_type (string): A string representing the artifact type.
            artifact_factory_func (Callable): A function to generate the
                artifact.
        """

        # Store the factory function in the registry so it can be called later.
        self.registry[artifact_type] = artifact_factory_func

    def register_factory(self, artifact_factory):
        """
        Register a factory class. This will tell the registry what artifact
        types the class supports.

        Args:
            artifact_factory: The factory to register.
        """

        # Discover artifact factory methods and register each artifact type
        # handled by this artifact factory.
        for name, method in inspect.getmembers(artifact_factory, inspect.ismethod):
            # Use the artifact_type decorator to identify supported artifact
            # types and their function methods.
            if hasattr(method, 'artifact_type'):
                self.register_artifact_type(method.artifact_type, method)

    def register_factories(self, artifact_factories, strategies):
        """
        Register artifact factory classes and the artifact types they support.

        Args:
            artifact_factories (list): The list of factories to register
            strategies (list): The list of supported strategies.
        """

        # Register each factory class.
        for factory_cls in artifact_factories.values():
            # Retrieve strategies required by the factory class.
            required_strategies = {
                strategy_bundle: strategies[strategy_bundle]
                for strategy_bundle in factory_cls.required_strategies
            }
            # Instantiate the artifact factory.
            artifact_factory = factory_cls(**required_strategies)
            # Register the artifact factory.
            self.register_factory(artifact_factory)

    def create_deployment(self, artifact_type, *args, **kwargs):
        """
        Create an artifact instance for deployment.

        Args:
            artifact_type (string): A string representing the artifact type.
            *args, **kwargs: Required arguments for the given artifact type.

        Returns:
            The generated artifact instance.
        """

        # Retrieve the artifact factory function from the registry.
        artifact_factory_func = self.registry[artifact_type]

        # Call the artifact type factory method, passing the arguments through
        # to the function.
        return artifact_factory_func(*args, **kwargs)

"""Print artifact plugin."""

from ...artifact import artifact_type, Artifact, ArtifactFactory
from ...plugin import plugin
from ...strategy import strategies
from ...template import DedentedTemplate, TrimmedTemplate, TemplateFromString

@plugin('artifact_factory', 'print')
@strategies('print_strategy')
class PrintArtifactFactory(ArtifactFactory):
    """
    Defines a class for creating print output artifacts.
    """

    def __init__(self, print_strategy):
        self.print_strategy = print_strategy

    def register(self, registry):
        registry.register_artifact_type('print', self.create_print)

    @artifact_type('print')
    def create_print(self, format, parameters, file):
        return PrintArtifact(
            format,
            parameters,
            file,
            self.print_strategy()
        )

class PrintArtifact(Artifact):
    """
    Defines a print artifact.
    """

    def __init__(self, format, parameters, file, print_strategy):
        self.format = format
        self.parameters = parameters
        self.file = file
        self.strategy = print_strategy

    def provision(self):
        self.strategy.provision(
            TrimmedTemplate(
                DedentedTemplate(
                    TemplateFromString(self.format)
                )
            ).render(self.parameters),
            self.file
        )

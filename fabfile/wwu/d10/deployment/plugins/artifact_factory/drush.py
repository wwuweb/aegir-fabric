"""Drush artifact plugin."""

from ...artifact import artifact_type, Artifact, ArtifactFactory
from ...plugin import plugin
from ...strategy import strategies
from ...template import TemplateFromFunction
from ....scaffold import drush_alias_factory

@plugin('artifact_factory', 'drush')
@strategies('remote_file')
class DrushArtifactFactory(ArtifactFactory):
    """
    Defines a class for creating drush alias artifacts.
    """

    def __init__(self, remote_file):
        self.remote_file = remote_file

    @artifact_type('drush_alias')
    def create_alias(self, subdomain, environment, environments, remote_path):
        return DrushAliasArtifact(
            subdomain,
            environment,
            environments,
            remote_path,
            self.remote_file()
        )

class DrushAliasArtifact(Artifact):
    """
    Defines a Drush alias artifact.
    """

    def __init__(
        self,
        subdomain,
        environment,
        environments,
        remote_path,
        remote_file_strategy
    ):
        self.subdomain = subdomain
        self.environment = environment
        self.environments = environments
        self.remote_path = remote_path
        self.strategy = remote_file_strategy

    def provision(self):
        self.strategy.provision(
            TemplateFromFunction(drush_alias_factory).render({
                'subdomain': self.subdomain,
                'environment': self.environment,
                'environments': self.environments
            }),
            self.remote_path
        )

"""Drupal artifact plugin."""

import os

from ...artifact import artifact_type, Artifact, ArtifactFactory
from ...plugin import plugin
from ...strategy import strategies
from ...template import DedentedTemplate, TemplateFromFile
from ....util import ReadonlyFile

@plugin('artifact_factory', 'drupal')
@strategies('remote_file')
class DrupalArtifactFactory(ArtifactFactory):
    """
    Defines a class for creating Drupal settings.php artifacts.
    """

    def __init__(self, remote_file):
        self.remote_file = remote_file

    @artifact_type('drupal_settings')
    def create_settings(
        self,
        template_path,
        drupal_root,
        sites_subdir,
        trusted_host_patterns,
        salt,
        database,
        username,
        password
    ):
        return DrupalSettingsArtifact(
            template_path,
            drupal_root,
            sites_subdir,
            trusted_host_patterns,
            salt,
            database,
            username,
            password,
            self.remote_file()
        )

class DrupalSettingsArtifact(Artifact):
    """
    Defines a Drupal settings.php file artifact.
    """

    def __init__(
        self,
        template_path,
        drupal_root,
        sites_subdir,
        trusted_host_patterns,
        salt,
        database,
        username,
        password,
        remote_file_from_template_strategy
    ):
        self.template_path = template_path
        self.drupal_root = drupal_root
        self.sites_subdir = sites_subdir
        self.trusted_host_patterns = trusted_host_patterns
        self.salt = salt
        self.database = database
        self.username = username
        self.password = password
        self.strategy = remote_file_from_template_strategy

    def provision(self):
        self.strategy.provision(
            DedentedTemplate(
                TemplateFromFile(
                    ReadonlyFile(self.template_path)
                )
            ).render({
                'trusted_host_patterns': self.trusted_host_patterns,
                'salt': self.salt,
                'database': self.database,
                'username': self.username,
                'password': self.password
            }),
            os.path.join(self.drupal_root, 'sites', self.sites_subdir, 'settings.php')
        )

"""MySQL artifact plugin."""

from ...artifact import artifact_type, Artifact, ArtifactFactory
from ...plugin import plugin
from ...strategy import strategies

@plugin('artifact_factory', 'mysql')
@strategies('mysql_command', 'mysql_secret_command')
class MySqlArtifactFactory(ArtifactFactory):
    """
    Defines a class for creating database-related artifacts.
    """

    def __init__(
        self,
        mysql_command,
        mysql_secret_command
    ):
        self.mysql_command = mysql_command
        self.mysql_secret_command = mysql_secret_command

    @artifact_type('mysql_database')
    def create_database(self, database, login_username, login_password):
        return MySqlDatabaseArtifact(
            database,
            self.mysql_command(
                login_username,
                login_password
            )
        )

    @artifact_type('mysql_user')
    def create_user(self, host, username, password, login_username, login_password):
        return MySqlUserArtifact(
            host,
            username,
            password,
            self.mysql_secret_command(
                login_username,
                login_password
            )
        )

    @artifact_type('mysql_grant')
    def create_grant(self, host, database, username, login_username, login_password):
        return MySqlGrantArtifact(
            host,
            database,
            username,
            self.mysql_command(
                login_username,
                login_password
            )
        )

class MySqlDatabaseArtifact(Artifact):
    """
    Defines a MySQL database artifact.

    Provisions a MySQL database. The target host is configured via the
    provisioning strategy, which may use a variety of implementations.
    """

    def __init__(self, database, mysql_strategy):
        self.database = database
        self.strategy = mysql_strategy

    def provision(self):
        self.strategy.provision(
            r'''CREATE DATABASE IF NOT EXISTS `{database}`'''.format(
                database=self.database
            )
        )

class MySqlUserArtifact(Artifact):
    """
    Defines a MySQL user artifact.

    Provisions a MySQL user. The host is required in order to fully qualify the
    user, enforcing the host from which they may connect to the MySQL instance;
    however, the provisioning strategy ultimately determines where the user is
    created.
    """

    def __init__(self, host, username, password, mysql_secret_strategy):
        self.host = host
        self.username = username
        self.password = password
        self.strategy = mysql_secret_strategy

    def provision(self):
        self.strategy.provision(
            r'''CREATE USER IF NOT EXISTS '{username}'@'{host}' IDENTIFIED BY '{password}';'''.format(
                username=self.username,
                host=self.host,
                password=self.password
            ),
            self.password
        )
        self.strategy.provision(
            r'''ALTER USER '{username}'@'{host}' IDENTIFIED BY '{password}';'''.format(
                username=self.username,
                host=self.host,
                password=self.password
            ),
            self.password
        )

class MySqlGrantArtifact(Artifact):
    """
    Defines a MySQL grant artifact.

    Grants full access permissions to the given user on the given database and
    host. The host is required in order to fully qualify the user, enforcing the
    host from which they may connect to the MySQL instance and on which host
    they will be granted permissions; however, the provisioning strategy
    ultimately determines where the grant is executed.
    """

    def __init__(self, host, database, username, mysql_strategy):
        self.host = host
        self.database = database
        self.username = username
        self.strategy = mysql_strategy

    def provision(self):
        self.strategy.provision(
            r''' GRANT ALL PRIVILEGES ON `{database}`.* to '{username}'@'{host}';'''.format(
                database=self.database,
                username=self.username,
                host=self.host
            )
        )

"""Fabric Remote File Strategy."""

import os

from fabric.api import hide, settings, sudo

from ...plugin import plugin
from ...strategy import Strategy

@plugin('strategy', 'remote_file')
class FabricRemoteFileStrategy(Strategy):
    """
    Defines a remote file strategy using the Fabric library.

    Generates a file on a remote server at the given path, populated with the
    given content and using the given remote generation strategy.
    """

    prompt = 'Enter content: '
    placeholder = 'CONTENT'

    def provision(self, content, remote_path):
        remote_directory = os.path.dirname(remote_path)
        with hide('warnings', 'running'):
            directory_exists = sudo('[ -d {remote_directory} ]'.format(
                remote_directory=remote_directory
            )).succeeded
        if not directory_exists:
            sudo('mkdir {remote_directory}'.format(
                remote_directory=remote_directory
            ))
        with settings(
            prompts={
                FabricRemoteFileStrategy.prompt: content + '\0'
            }
        ), hide('output'):
            # For the meaning of the "-d" flag below, see:
            # https://www.gnu.org/software/bash/manual/html_node/ANSI_002dC-Quoting.html
            sudo(r'''read -s -r -d $'\0' -p "{prompt}" {placeholder}; printf "%s" "${placeholder}" > {remote_path}'''.format(
                prompt=FabricRemoteFileStrategy.prompt,
                placeholder=FabricRemoteFileStrategy.placeholder,
                remote_path=remote_path,
            ))

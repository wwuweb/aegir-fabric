"""Print Strategy."""

from __future__ import print_function

from ...plugin import plugin
from ...strategy import Strategy

@plugin('strategy', 'print_strategy')
class PrintStrategy(Strategy):
    """
    Defines a print artifact.

    Prints the given content to the given file, which may be a file or file-like
    object.
    """

    def provision(self, content, file):
        print(content, file=file)

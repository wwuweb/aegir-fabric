"""Local File Strategy."""

import errno, os

from ...plugin import plugin
from ...strategy import Strategy

@plugin('strategy', 'local_file')
class LocalFileStrategy(Strategy):
    """
    Defines a local file artifact.

    Generates a file, populated with the given content, on the local filesystem
    at the given filepath, creating the parent directory if needed.
    """

    def __init__(self, filepath):
        self.filepath = filepath

    def provision(self, content):
        dirname = os.path.dirname(self.filepath)
        try:
            os.makedirs(dirname)
        except OSError, e:
            if e.errno != errno.EEXIST:
                raise
            pass
        with open(self.filepath, 'w') as file:
            file.write(content)

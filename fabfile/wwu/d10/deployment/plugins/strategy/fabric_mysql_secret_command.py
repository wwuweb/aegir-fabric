"""Fabric MySql Secret Command Strategy."""

from fabric.api import hide, settings, sudo

from .fabric_mysql_command import FabricMySqlCommandStrategy
from ...plugin import plugin
from ...strategy import Strategy

@plugin('strategy', 'mysql_secret_command')
class FabricMySqlSecretCommandStrategy(Strategy):
    """
    Defines an SQL command artifact with an interpolated secret, using the
    Fabric library.

    Uses the mysql commandline utility to run the given SQL command on a remote
    host. This artifact should be used if the command will contain a sensitive
    payload, such as a user password. This "secret" must be passed to the
    constructor so that it can be securely inserted into the command at runtime.
    """

    secret_prompt = 'Enter secret: '
    secret_placeholder = 'SECRET'

    def __init__(self, username, password):
        self.username = username
        self.password = password

    def provision(self, content, secret):
        with settings(
            prompts={
                FabricMySqlCommandStrategy.password_prompt: self.password,
                FabricMySqlSecretCommandStrategy.secret_prompt: secret,
            }
        ), hide('output'):
            sudo(r'''read -s -p \"{secret_prompt}\" {secret_placeholder}; mysql --user {username} --password --execute \"{command}\"'''.format(
                secret_prompt=FabricMySqlSecretCommandStrategy.secret_prompt,
                secret_placeholder=FabricMySqlSecretCommandStrategy.secret_placeholder,
                username=self.username,
                command=content.replace(
                    '''\\''', '''\\\\'''
                ).replace(
                    r'''`''', r'''\\`'''
                ).replace(
                    r'''"''', r'''\\"'''
                ).replace(
                    r'''$''', r'''\\$'''
                ).replace(
                    secret, r'''\${secret_placeholder}'''.format(
                        secret_placeholder=FabricMySqlSecretCommandStrategy.secret_placeholder
                    )
                ),
            ), shell_escape=False)

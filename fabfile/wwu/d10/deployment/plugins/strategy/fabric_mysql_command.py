"""Fabric MySql Command Strategy."""

from fabric.api import hide, settings, sudo

from ...plugin import plugin
from ...strategy import Strategy

@plugin('strategy', 'mysql_command')
class FabricMySqlCommandStrategy(Strategy):
    """
    Defines an SQL command strategy using the Fabric library.

    Uses the mysql commandline utility to run the given SQL command on a remote
    host. The SQL user credentials must be provided as string arguments. The
    database host information is handled in the Fabric configuration layer.

    This artifact should not be used to execute commands containing sensitive
    data, such as passwords.

    See: MySqlSecretCommandArtifact().
    """

    password_prompt = 'Enter password: '

    def __init__(self, username, password):
        self.username = username
        self.password = password

    def provision(self, content):
        with settings(
            prompts={
                FabricMySqlCommandStrategy.password_prompt: self.password
            }
        ), hide('output'):
            sudo(r'''mysql --user {username} --password --execute "{command}"'''.format(
                username=self.username,
                command=content.replace(
                    r'''`''', r'''\\`'''
                ).replace(
                    r'''"''', r'''\\"'''
                ).replace(
                    r'''$''', r'''\\$'''
                ),
            ))


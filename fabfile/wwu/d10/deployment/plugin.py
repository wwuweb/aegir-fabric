"""Plugin management library."""

import glob, importlib, inspect, os

class plugin:
    """
    Decorator to mark a class as a plugin.

    Args:
        plugin_type (str): Plugins types provided by the framework (e.g.
            'strategy', 'artifact_factory').
        plugin_bundle (str): Pluggable bundles that can accept multiple
            implementations.

    Returns:
        callable: The decorated plugin class.
    """

    def __init__(self, plugin_type, plugin_bundle):
        self.plugin_type = plugin_type
        self.plugin_bundle = plugin_bundle

    def __call__(self, cls):
        cls.plugin_type = self.plugin_type
        cls.plugin_bundle = self.plugin_bundle
        return cls

class PluginDiscovery:
    """
    Base plugin discovery class.
    """

    plugin_type = None

    def discover(self):
        """
        Discover plugins of the given type.

        Plugins are expected to be under the 'plugins' directory relative to
        this module, organized in sub-directories according to plugin type.

        Return:
            A dictionary of plugin classes keyed by plugin bundle.
        """

        plugins = {}
        # Get the absolute path to the plugins directory.
        plugins_dir = os.path.join(
            os.path.dirname(os.path.abspath(__file__)), 'plugins'
        )
        # Iterate over all plugin files within the specified directory.
        for filepath in glob.glob(
            os.path.normpath(os.path.join(plugins_dir, self.plugin_type, '*.py'))
        ):
            # Derive the module name from the filename.
            module_name, ext = os.path.splitext(os.path.basename(filepath))
            # Import and load the module dynamically.
            module = importlib.import_module(
                '.plugins.%s.%s' % (self.plugin_type, module_name),
                package=__package__
            )
            # Inspect the module for decorated plugin classes.
            for name, cls in inspect.getmembers(module, inspect.isclass):
                if (
                    hasattr(cls, 'plugin_type') and
                    hasattr(cls, 'plugin_bundle') and
                    cls.plugin_type == self.plugin_type
                ):
                    plugins[cls.plugin_bundle] = cls
        return plugins

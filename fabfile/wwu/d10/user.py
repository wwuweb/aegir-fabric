"""User management tasks."""

import ldap

from fabric.api import abort, env, execute, puts, task, warn
from datetime import datetime, timedelta
from urlparse import urlparse

from ... import active_directory
from . import alias

from ...user import user_authinfo, count_users
from ...config import config_get, config_set

from .pm import package_enabled

@task
def block_user_1_all():
    """Block user 1 on all sites."""

    site_records = alias.load_all()

    for site_alias in site_records:
        execute('user.block', site_alias, 1)

@task
def user_block_all(user):
    """Block given user on all sites."""

    site_records = alias.alias.load_all()

    for site_alias in site_records:
        execute('user.block', site_alias, user)

@task
def user_unblock_all(user):
    """Unblock given user on all sites."""

    site_records = alias.alias.load_all()

    for site_alias in site_records:
        execute('user.unblock', site_alias, user)

@task
def update_ldap_password(username, password):
    """Update the ldap password for all sites on the given platform."""

    name = 'ldap_servers.server.western_ldap'
    site_records = alias.load_all()

    for site_alias in site_records:
        try:
            if config_get(site_alias, name) and package_is_enabled(site_alias, 'ldap_servers'):
                config_set(site_alias, name, 'binddn', username)
                config_set(site_alias, name, 'bindpw', password)
        except Exception as e:
            warn(str(e))
@task
def user_list_all():
    """List users for all sites."""

    environment = env.aegir['host']['environment']
    basedn = env.aegir['ldap']['basedn']
    binddn = active_directory.binddn()
    cred = env.aegir['ldap']['cred']
    scope = ldap.SCOPE_SUBTREE
    attributes = ['mail']

    try:
        ad = ldap.initialize(env.aegir['ldap']['uri'])
        ad.protocol_version = ldap.VERSION3
        ad.simple_bind_s(binddn, cred)
    except Exception as e:
        abort(str(e))

    site_records = alias.load_all()
    output = open("editors_%s.csv" % environment, 'a')
    limit = 100
    roles = ['editor', 'super_editor', 'administrator']
    output.write("site,email,uid\n")

    for site_alias in site_records:
        count = count_users(site_alias)
        parsed_uri = urlparse(site_records[site_alias]['uri'])
        puts("%s:" % parsed_uri.netloc)

        for offset in range(0, count, limit):
            accounts = {
                account['uid']: account['authname']
                for account in user_authinfo(site_alias, limit, offset)
                if 'authname' in account
                if account['status'] == '1'
                if any(role in roles for role in account['roles'].split(','))
            }

            for uid in accounts:
                search = "(samaccountname=%s)" % accounts[uid]

                try:
                    result_id = ad.search(basedn, scope, search, attributes)
                    result_type, result_data = ad.result(result_id, all=1)

                    if result_data and 'mail' in result_data[0][1]:
                        email = result_data[0][1]['mail'][0]
                        puts("  - %s" % email)
                        output.write("%s,%s,%s\n" % (parsed_uri.netloc, email, uid))
                except Exception as e:
                    warn(str(e))

    ad.unbind()
    output.close()

@task
def user_editor_emails_all():
    """List users for all sites."""

    environment = env.aegir['host']['environment']
    basedn = env.aegir['ldap']['basedn']
    binddn = active_directory.binddn()
    cred = env.aegir['ldap']['cred']
    scope = ldap.SCOPE_SUBTREE
    attributes = ['mail']
    webtech = active_directory.groupdn(env.aegir['ldap']['groups']['webtech'])

    try:
        ad = ldap.initialize(env.aegir['ldap']['uri'])
        ad.protocol_version = ldap.VERSION3
        ad.simple_bind_s(binddn, cred)
    except Exception as e:
        abort(str(e))

    site_records = alias.load_all()
    emails = set()
    output = open("emails_%s.txt" % environment, 'a')
    limit = 100
    cutoff = datetime.now() - timedelta(days=90)
    roles = ['editor', 'super_editor', 'administrator']

    for site_alias in site_records:
        count = count_users(site_alias)
        puts("%s:" % site_alias)

        for offset in range(0, count, limit):
            accounts = [
                "(samaccountname=%s)" % account['authname']
                for account in user_authinfo(site_alias, limit, offset)
                if 'authname' in account
                if account['status'] == '1'
                if any(role in roles for role in account['roles'].split(','))
                if datetime.fromtimestamp(int(account['access'])) > cutoff
            ]

            if not accounts:
                continue

            search = "(&(|%s)(!(memberof=%s)))" % (''.join(accounts), webtech)

            try:
                result_id = ad.search(basedn, scope, search, attributes)

                while True:
                    result_type, result_data = ad.result(result_id, all=0)

                    if result_type == ldap.RES_SEARCH_RESULT:
                        break;
                    else:
                        email = result_data[0][1]['mail'][0]
                        puts("  - %s" % email)
                        emails.add(email)
            except Exception as e:
                warn(str(e))

    ad.unbind()
    output.write(';'.join(emails))
    output.close()

@task
def user_normalize_emails_all():
    """Set email to username@wwu.edu for all sites."""

    environment = env.aegir['host']['environment']
    basedn = env.aegir['ldap']['basedn']
    binddn = env.aegir['ldap']['binddn']
    cred = env.aegir['ldap']['cred']
    scope = ldap.SCOPE_SUBTREE
    attributes = ['mail']

    try:
        ad = ldap.initialize(env.aegir['ldap']['uri'])
        ad.protocol_version = ldap.VERSION3
        ad.simple_bind_s(binddn, cred)
    except Exception as e:
        abort(str(e))

    site_records = alias.load_all()
    emails = set()
    output = open("emails_%s.txt" % environment, 'a')
    limit = 100
    cutoff = datetime.now() - timedelta(days=90)
    roles = ['editor', 'super_editor', 'administrator']

    for site_alias in site_records:
        count = count_users(site_alias)
        puts("%s:" % site_alias)

        for offset in range(0, count, limit):
            authinfo = user_authinfo(site_alias, limit, offset)

            accounts = [
                account for account in authinfo
                if account['status'] == '1'
                if datetime.fromtimestamp(int(account['access'])) > cutoff
                if any(role in roles for role in account['roles'].split(','))
            ]

            for account in accounts:
                search = "(samaccountname=%s)" % account['authname']

                try:
                    result_id = ad.search(basedn, scope, search, attributes)
                    result_type, result_data = ad.result(result_id, all=1)

                    if result_data and 'mail' in result_data[0][1]:
                        email = result_data[0][1]['mail'][0]
                        puts(" - {")
                        puts("     authname: %s" % account['authname'])
                        puts("     email: %s" % email)
                        puts("   }")
                        emails.add(email)
                except Exception as e:
                    warn(str(e))

    ad.unbind()
    output.write(';'.join(emails))
    output.close()

"""Drupal core tasks."""

from fabric.api import abort, execute, hide, puts, task

from ... import drush
from . import alias

@task
def cron(site_alias):
    """Run cron for the given site."""

    drush.run(site_alias, 'core:cron')

@task
def cron_all():
    """Run cron for all sites."""

    site_records = alias.load_all()

    for site_alias in site_records:
        with hide('running'):
            puts("Running cron for %s..." % site_records[site_alias]['uri'])
            execute('build_server.build_job', 'core-cron', {
                'site_alias': site_alias
            })

@task
def requirements(site_alias):
    """Get the core requirements for the given site."""

    drush.run(site_alias, 'core:requirements', '--format', 'yaml')

@task
def requirements_all():
    """Get the requirements for all sites."""

    site_records = alias.load_all()

    for site_alias in site_records:
        with hide('running'):
            execute('build_server.build_job', 'core-requirements', {
                'site_alias': site_alias
            })

@task
def status(site_alias):
    """Get the core status for the given site."""

    output = drush.run(site_alias, 'core:status', '--format', 'yaml')

    if not 'db-status' in output or output['db-status'] != 'Connected':
        abort('Database connection not established.')

    if not 'bootstrap' in output or output['bootstrap'] != 'Successful':
        abort('Drupal not bootstrapped.')

@task
def status_all():
    """Get the status of all sites."""

    site_records = alias.load_all()

    for site_alias in site_records:
        with hide('running'):
            puts("Checking status for %s..." % site_records[site_alias]['uri'])
            execute('build_server.build_job', 'core-status', {
                'site_alias': site_alias
            })

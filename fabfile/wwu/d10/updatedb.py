"""Database update tasks."""

import string
import textwrap

from fabric.api import abort, execute, hide, puts, task, warn, env

from ... import drush
from . import alias

@task
def system_schema_delete(site_alias, module):
    """Delete the given module from the system schema key-value store."""

    template= """
        "\Drupal::keyValue('system.schema')->delete('{module}')"
    """
    parameters = { 'module': module }
    command = textwrap.dedent(template.format(**parameters)).strip()
    drush.run(site_alias, 'php:eval', command)

@task
def system_schema_delete_all(module):
    """Delete the given module from the system schema for all sites."""

    site_records = alias.load_all()

    for site_alias in site_records:
        execute(system_schema_delete, site_alias, module)

@task
def core_extension_delete(site_alias, package, type='module'):
    """Delete the given package from core.extension config."""

    template = """
        "\\\$config = \\Drupal::config('core.extension')->get('{type}'); unset(\\\$config['{package}']); \\Drupal::configFactory()->getEditable('core.extension')->set('{type}', \\\$config)->save();"
    """
    parameters = { 'package': package, 'type': type }
    command = textwrap.dedent(template.format(**parameters)).strip()
    drush.run(site_alias, 'php:eval', command)

@task
def core_extension_delete_all(package, type='module'):
    """Delete the given package from core.extension for all sites."""

    site_records = alias.load_all()

    for site_alias in site_records:
        execute(core_extension_delete, site_alias, package, type)

@task
def get_installed_version(site_alias, package):
    """Get the installed version of the given package."""

    template = """
        "echo \Drupal::service('update.update_hook_registry',)->getInstalledVersion('{package}')"
    """
    parameters = { 'package': package }
    command = textwrap.dedent(template.format(**parameters)).strip()
    drush.run(site_alias, 'php:eval', command)

@task
def set_installed_version(site_alias, package, version):
    """Set the installed version of the given package."""

    template = """
        "\Drupal::service('update.update_hook_registry',)->setInstalledVersion('{package}', '{version}')"
    """
    parameters = { 'package': package, 'version': version }
    command = textwrap.dedent(template.format(**parameters)).strip()
    drush.run(site_alias, 'php:eval', command)

@task
def get_installed_version_all(package):
    """Get the installed version of the given package on all sites."""

    site_records = alias.load_all()

    for site_alias in site_records:
        execute(get_installed_version, site_alias, package)

@task
def set_installed_version_all(package, version):
    """Set the installed version of the given package on all sites."""

    site_records = alias.load_all()

    for site_alias in site_records:
        execute(set_installed_version, site_alias, package, version)

@task
def status(site_alias):
    """Get the status of database updates."""

    output = drush.run(site_alias, 'updatedb:status')

    if not 'No database updates required' in output:
        abort('Database updates pending.')

    if 'warning' in output or 'error' in output or output.failed:
        abort('Problems were found while checking for database updates.')

@task
def status_all():
    """Get database update status for all sites."""

    site_records = alias.load_all()

    for site_alias in site_records:
        with hide('running'):
            execute('build_server.build_job', 'updatedb-status', {
                'site_alias': site_alias
            })

@task
def updatedb(site_alias):
    """Run database updates on the given site."""

    output = drush.run(site_alias, 'updatedb')

    if 'warning' in output or 'error' in output or output.failed:
        abort('Database updates failed.')

@task
def updatedb_all():
    """Run database updates on all sites."""

    site_records = alias.load_all()

    for site_alias in site_records:
        with hide('running'):
            execute('build_server.build_job', 'updatedb', {
                'site_alias': site_alias
            })

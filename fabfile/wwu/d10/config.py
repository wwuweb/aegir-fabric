"""Config tasks."""

from fabric.api import execute, task

from . import alias
from ...config import config_set, config_delete
from ...config_update import config_import, config_revert

@task
def set_all(config_name, key, value):
    """Set the given config for all sites."""

    site_records = alias.load_all()

    for site_alias in site_records:
        config_set(site_alias, config_name, key, value)

@task
def import_all(config_name):
    """Import config for all sites."""

    site_records = alias.load_all()

    for site_alias in site_records:
        config_import(site_alias, config_name)

@task
def revert_all(config_name):
    """Revert config for all sites."""

    site_records = alias.load_all()

    for site_alias in site_records:
        config_revert(site_alias, config_name)

@task
def delete_all(config_name):
    """Delete config for all sites."""

    site_records = alias.load_all()

    for site_alias in site_records:
        config_delete(site_alias, config_name)

@task
def disable_alert_display(site_alias):
    """Disable alert display in ashlar."""

    config_set(site_alias, 'ashlar.settings', 'alert_display', 0)

@task
def enable_alert_display(site_alias):
    """Enable alert display in ashlar."""

    config_set(site_alias, 'ashlar.settings', 'alert_display', 1)

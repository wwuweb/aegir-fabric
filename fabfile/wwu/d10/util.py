"""Utilities library"""

class ReadonlyFile:
    """
    Defines a file that is opened read-only.

    The path can be a filesystem path or a file descriptor on the system.
    """

    def __init__(self, filepath):
        self.filepath = filepath

    def read(self):
        with open(self.filepath, 'r') as file:
            return file.read()

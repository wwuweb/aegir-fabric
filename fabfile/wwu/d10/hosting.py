"""Infrastructure maintenance tasks for drupal sites."""

import os, yaml

from fabric.api import abort, hide, puts, sudo, task

from ... import drush
from . import alias

from .deployment import Deployment

def _get_trusted_host_patterns(site_alias):
    """Get the trusted host patterns for the given site."""

    command = r"""
        'echo \Drupal\Component\Serialization\Yaml::encode(\Drupal\Core\Site\Settings::get("trusted_host_patterns"));'
    """

    with hide('output'):
        output = drush.run(site_alias, 'php:eval', command)

    try:
        trusted_host_patterns = ",\n".join("  '%s'" % pattern for pattern in yaml.load(output))
    except Exception as e:
        abort(str(e))

    return trusted_host_patterns

def _get_salt(site_alias):
    """Get the hash salt for the given site."""

    command = r"""
        'echo \Drupal\Component\Serialization\Yaml::encode(\Drupal\Core\Site\Settings::get("hash_salt"));'
    """

    with hide('output'):
        output = drush.run(site_alias, 'php:eval', command)

    try:
        salt = yaml.load(output)
    except Exception as e:
        abort(str(e))

    return salt

@task
def settings_php_update(site_alias):
    """Update the settings file for the given site."""

    trusted_host_patterns = _get_trusted_host_patterns(site_alias)
    salt = _get_salt(site_alias)

    with hide('output'):
        output = drush.run(
            site_alias, 'core:status', '--format=yaml', '--fields=db-name,db-username,db-password,site,root'
        )

    sudo('chmod 775 {site_path}'.format(
        site_path=os.path.join(output['root'], output['site'])
    ))

    sudo('chmod 664 {settings_file_path}'.format(
        settings_file_path=os.path.join(output['root'], output['site'], 'settings.php')
    ))

    deployment = Deployment()

    drupal_settings = deployment.create_deployment(
        'drupal_settings',
        os.path.join(
            os.path.dirname(os.path.abspath(__file__)), 'deployment', 'templates', 'settings.tpl'
        ),
        output['root'],
        os.path.basename(output['site']),
        trusted_host_patterns,
        salt,
        output['db-name'],
        output['db-username'],
        output['db-password']
    )

    drupal_settings.provision()

    sudo('chmod 555 {site_path}'.format(
        site_path=os.path.join(output['root'], output['site'])
    ))

    sudo('chmod 444 {settings_file_path}'.format(
        settings_file_path=os.path.join(output['root'], output['site'], 'settings.php')
    ))

@task
def settings_php_update_all():
    """Update the settings file for all sites."""

    site_records = alias.load_all()

    for site_alias in site_records:
        with hide('running'):
            puts("Updating settings.php for %s..." % site_records[site_alias]['uri'])
            settings_php_update(site_alias)

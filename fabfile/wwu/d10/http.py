"""Site health status tasks."""

import requests

from fabric.api import abort, execute, hide, puts, task
from requests import ConnectionError

from ... import drush
from . import alias

@task
def status(site_alias):
    """Get the HTTP status of a single site."""

    site_record = alias.load(site_alias)
    uri = site_record[site_alias]['uri']

    try:
        response = requests.get(uri)
    except ConnectionError as exception:
        abort("Request failed with connection error:\n%s" % exception)

    if not response or not response.status_code or not response.status_code == 200:
        abort("Request to %s returned %s." % (uri, response.status_code))

    if 'Unexpected Error' in response.text:
        abort("The site at %s encountered an unexpected error." % uri)

    puts("Request to %s returned %s." % (uri, response.status_code))

@task
def status_all():
    """Get the status of all sites."""

    site_records = alias.load_all()

    for site_alias in site_records:
        with hide('running'):
            puts("Checking status for %s..." % site_records[site_alias]['uri'])
            execute('build_server.build_job', 'http-status', {
                'site_alias': site_alias
            })

"""Drupal scafold library."""

import re, string, textwrap

from random import SystemRandom

class CachedManifest:
    """Caches the values returned from the wrapped Manifest."""

    def __init__(self, manifest):
        self.manifest = manifest
        self.cache = {}

    def __getattr__(self, name):
        return self._cached_access(name)

    def __getitem__(self, key):
        return self._cached_access(key)

    def keys(self):
        return self.manifest.keys()

    def attribute_map(self):
        return self.manifest.attribute_map()

    def _cached_access(self, name):
        if not name in self.cache:
            self.cache[name] = getattr(self.manifest, name)
        return self.cache[name]

class Manifest:
    """
    Configuration object for scaffolding Drupal sites.

    Relies on YAML configuration defined in settings/scaffold.yml:

        'environments':
          <environment-1>:
            ...
          ...
          <environment-n>:
            ...

    The top-level key must be 'environments'. This key should contain a
    dictionary of environments, keyed by the name of the environment ('prod',
    'dev', 'test', etc). Each key should in turn be a dictionary that defines
    the configuration for that environment with the following keys:
        - environment: The label for the environment (e.g.: dev, test, prod).
        - host: The fully-qualified domain name of the host.
        - root: The file system path where the drupal web root is located (e.g.:
          /path/to/drupal/web).
        - user: The user Drush will use to log in to the remote host.
        - uri: Python format string template for URIs in the environment. (.e.g:
          "https://{subdomain}AAA.XXX.YYY"). It should accept one paramter named
          "subdomain", indicated by curly braces.

    A full definition might look like the following:

        'environments':
          'prod':
            'environment': 'prod'
            'host': 'drupal.prod.host'
            'root': '/path/to/drupal/root'
            'user': 'drupal_user'
            'uri': 'https://{subdomain}prod.domain.tld'
          'dev':
            'environment': 'dev'
            'host': 'drupal.dev.host'
            'root': '/path/to/drupal/root'
            'user': 'drupal_user'
            'uri': 'https://{subdomain}dev.domain.tld'

    This file will be loaded into the fabric shared 'env' dictionary under the
    key 'aegir/scaffold/environments' via the code in settings.py. The
    Manifest class will read from this location for the loaded config and
    self-populate its attributes accordingly.

    """

    password_length = 32
    hash_salt_length = 74

    def __init__(
        self,
        subdomain,
        environment,
        environments,
        hostname_factory,
        database_name_factory,
        secure_random_string_factory,
        trusted_host_patterns_factory
    ):
        self.subdomain = subdomain
        self.environment = environment
        self.environments = environments
        self.hostname_factory = hostname_factory
        self.database_name_factory = database_name_factory
        self.secure_random_string_factory = secure_random_string_factory
        self.trusted_host_patterns_factory = trusted_host_patterns_factory

    def __getattr__(self, name):
        factory = self.attribute_map().get(name)
        return factory()

    def __getitem__(self, key):
        try:
            return getattr(self, key)
        except AttributeError as e:
            raise KeyError(str(e))

    def keys(self):
        return self.attribute_map().keys()

    def attribute_map(self):
        return {
            # The fully-qualifed domain name of the host server.
            'host': lambda: (
                self.environments[self.environment]['host']
            ),
            # The fully-qualified URI of the drupal site, including protocol.
            'uri': lambda: (
                self.environments[self.environment]['uri'].format(subdomain=self.subdomain)
            ),
            # The root directory of the drupal installation (usually ending in /web).
            'drupal_root': lambda: (
                self.environments[self.environment]['root']
            ),
            # A regex array matching the trusted host patterns for the drupal site.
            'trusted_host_patterns': lambda: (
                self.trusted_host_patterns_factory([self.uri])
            ),
            # The name of the directory for the drupal site (located under /web/sites).
            'sites_subdir': lambda: (
                self.hostname_factory(self.uri)
            ),
            # The bare hostname of the site, as derived from the URI.
            'hostname': lambda: (
                self.hostname_factory(self.uri)
            ),
            # The name of the database.
            'database': lambda: (
                self.database_name_factory(self.uri)
            ),
            # The database user.
            'username': lambda: (
                self.database_name_factory(self.uri)
            ),
            # The database password.
            'password': lambda: (
                self.secure_random_string_factory(Manifest.password_length)
            ),
            # The hash salt for the drupal site.
            'salt': lambda: (
                self.secure_random_string_factory(Manifest.hash_salt_length)
            ),
            # The subdomain of the drupal site.
            'subdomain': lambda: (
                self.subdomain
            ),
            # The environment where the drupal site will be deployed.
            'environment': lambda: (
                self.environment
            ),
            # The environments dictionary.
            'environments': lambda: (
                self.environments
            )
        }

class ManifestFactory:
    """
    Defines a class for generating a Drupal manifest.
    """

    def __call__(self, subdomain, environment, environments):
        return CachedManifest(
            Manifest(
                subdomain,
                environment,
                environments,
                hostname_factory,
                database_name_factory,
                SecureRandomStringFactory(
                    string.letters + string.digits,
                    SystemRandom().choice
                ),
                TrustedHostPatternsFactory(
                    hostname_factory
                )
            )
        )

class SecureRandomStringFactory:
    """
    Class for generating cryptographically secure psuedo-random strings.

    Accepts a string that will be used as the character set sequence from which
    the random string will be generated.

    The choice parameter must accept this sequence as an argument when called,
    e.g. SystemRandom().choice.

    The return value is a psudeo-random string.
    """

    def __init__(self, sequence, choice):
        self.sequence = sequence
        self.choice = choice

    def __call__(self, length):
        return ''.join([
            self.choice(self.sequence)
            for _ in xrange(length)
        ])

class TrustedHostPatternsFactory:
    """Class for generating trusted host patterns."""

    def __init__(self, hostname_factory):
        self.hostname_factory = hostname_factory

    def __call__(self, uris):
        """
        Generate a trusted host pattern string from the given array of URIs.
        """

        return ",\n".join([
            "  '^" + re.escape(self.hostname_factory(uri)) + "$'"
            for uri in uris
        ])

def database_name_factory(uri):
    """Suggest a database name and username for a site from the given URI."""

    return re.sub('[^A-Za-z0-9]|^https?', '', uri)

def drush_alias_factory(subdomain, environment, environments):
    """
    Generate a complete drush alias for the given subdomain and environments.

    The generated alias will have a key for each environment in the dictionary
    "environments". The "local" (current) environment identified by the
    "environment" argument will be defined as a "local" alias, meaning it will
    not have "host" or "user" keys, which are required for aliases definining a
    site on a remote machine.
    """

    return "\n".join([
        textwrap.dedent(
            """
            {environment}:
              root: {root}
              uri: {uri}
            """.format(**{
                'environment': environment,
                'root': value['root'],
                'uri': value['uri'].format(**{'subdomain': subdomain})
            })
        ).strip()
        if key.lower() == environment.lower() else
        textwrap.dedent(
            """
            {environment}:
              host: {host}
              user: {user}
              root: {root}
              uri: {uri}
            """.format(**{
                'environment': key,
                'host': value['host'],
                'user': value['user'],
                'root': value['root'],
                'uri': value['uri'].format(**{'subdomain': subdomain})
            })
        ).strip()
        for key, value in environments.items()
    ])

def hostname_factory(uri):
    """Get the hostname from the given URI."""

    return re.sub('^https?\:\/\/', '', uri)

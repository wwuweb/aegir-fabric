"""Backup tasks."""

from fabric.api import abort, execute, get, sudo, task
from datetime import datetime

from ... import drush
from . import alias

@task
def site(site_alias):
    """Back up the given site."""

    site_record = alias.load(site_alias)
    db_name = site_record['db-name']
    root = site_record['root']
    site = site_record['site']

    with cd('/var/aegir/backup'):
        sudo('%s sql:dump %s.sql' % (site_alias, db_name))
        sudo('rsync -a %s/%s/ %s' % (root, site, db_name))
        sudo('tar -czf %s.tgz %s %s.sql' % (db_name, db_name, db_name))
        sudo('chmod -R g+w %s' % db_name)
        sudo('rm -r %s %s.sql' % (db_name, db_name))

@task
def backup_all():
    """Run backup for all sites on the given platform."""

    site_records = alias.load_all()

    for site_alias in site_records:
        label = site_alias.replace('@', '')
        timestamp = datetime.today().strftime('%Y-%m-%d-%H%M')
        basename = "%s-%s.sql" % (label, timestamp)
        dirname = '/tmp'
        path = "%s/%s" % (dirname, basename)

        execute('sql.dump', site_alias, path)
        get(path, "%(basename)s")
        sudo("rm -fv %s" % path)

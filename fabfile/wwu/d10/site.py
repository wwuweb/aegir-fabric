"""Site provisioning tasks."""

import os, sys

from fabric.api import env, task

from .deployment import Deployment
from .scaffold import ManifestFactory
from .util import ReadonlyFile

@task
def provision(subdomain):
    """
    Deploy scaffolding necessary for a Drupal site install on a remote
    server.

    This task performs the following operations:

    1. Create a database.
    2. Create a database user.
    3. Update the database user password to ensure it matches the value in
       settings.php.
    4. Grant database privileges to the user.
    5. Create a settings.php file.
    6. Create a drush alias file.
    7. Suggest (but do not execute) a drush site-install command.

    This taks requires configuration stored in the Fabric shared "env"
    dictionary, which is a feature of the Fabric library. This dictionary is
    populated by custom code defined in settings.py, looking for files defined
    in the config directory of this project.
    """

    manifest_factory = ManifestFactory()

    manifest = manifest_factory(
        subdomain,
        env['aegir']['host']['environment'],
        env['aegir']['scaffold']['environments']
    )

    aegir_user = env['aegir']['host']['aegir_user']

    database_credentials_file = ReadonlyFile(os.environ['database_credentials_file'])
    database_root_username = env['aegir']['host']['database_user']
    database_root_password = database_credentials_file.read().strip()

    deployment = Deployment()

    artifacts = [
        deployment.create_deployment(
            'mysql_database',
            manifest.database,
            database_root_username,
            database_root_password
        ),
        deployment.create_deployment(
            'mysql_user',
            manifest.host,
            manifest.username,
            manifest.password,
            database_root_username,
            database_root_password
        ),
        deployment.create_deployment(
            'mysql_grant',
            manifest.host,
            manifest.database,
            manifest.username,
            database_root_username,
            database_root_password
        ),
        deployment.create_deployment(
            'drupal_settings',
            os.path.join(
                os.path.dirname(os.path.abspath(__file__)), 'deployment', 'templates', 'settings.tpl'
            ),
            manifest.drupal_root,
            manifest.sites_subdir,
            manifest.trusted_host_patterns,
            manifest.salt,
            manifest.database,
            manifest.username,
            manifest.password
        ),
        deployment.create_deployment(
            'drush_alias',
            manifest.subdomain,
            manifest.environment,
            manifest.environments,
            os.path.join('/home', aegir_user, '.drush/sites/wwu', subdomain + '.site.yml')
        ),
        deployment.create_deployment(
            'print',
            """
            drush @wwu.{subdomain}.{environment} site:install --sites-subdir {sites_subdir}
            """,
            manifest,
            sys.stdout
        ),
    ]

    for artifact in artifacts:
        artifact.provision()

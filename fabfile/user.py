"""User management tasks."""

from fabric.api import abort, hide, task

from . import drush

def count_users(site_alias):
    """Get the count of users on the given site."""

    query =  "'SELECT COUNT(uid) FROM users WHERE uid <> 0'"

    with hide('running', 'output'):
        output = drush.run(site_alias, 'sql:query', query)

    if output.failed:
        abort(output)

    return int(output)

def uids(site_alias, limit, offset):
    """Get uids from the given site, with limit and offset."""

    query = "'SELECT uid FROM users WHERE uid <> 0 ORDER BY uid LIMIT %s OFFSET %s'"

    with hide('running', 'output'):
        output = drush.run(site_alias, 'sql:query', query % (limit, offset))

    return [s for s in output.splitlines() if s.strip()]

def user_info(site_alias, users):
    """Get user information for the given uids."""

    if type(users) is list:
        users = ','.join(users)

    with hide('running', 'output'):
        output = drush.run(site_alias, '--format=yaml', 'user:information', users)

    return output

def user_authinfo(site_alias, limit, offset):
    """Get uids and auth info for the given site alias."""

    query = """'SELECT
                    users_field_data.uid,
                    name,
                    mail,
                    authname,
                    access,
                    status,
                    GROUP_CONCAT(roles_target_id) AS roles
                FROM
                    users_field_data,
                    authmap,
                    user__roles
                WHERE
                    users_field_data.uid = authmap.uid
                AND
                    users_field_data.uid = user__roles.entity_id
                GROUP BY
                    uid
                ORDER BY
                    uid
                LIMIT
                    %s
                OFFSET
                    %s;'"""

    with hide('running', 'output'):
        output = drush.run(site_alias, 'sql:query', query % (limit, offset))

    columns = ['uid', 'name', 'mail', 'authname', 'access', 'status', 'roles']

    return [dict(zip(columns, line.split('\t'))) for line in output.splitlines() if line.strip()]

@task
def block(site_alias, users):
    """Block the given users."""

    if type(users) is list:
        users = ','.join(users)

    return drush.run(site_alias, 'user:block', users)

@task
def block_batch(site_alias, users):
    """Block the given users, passed as a multiline string."""

    for user in users.splitlines():
        block(site_alias, user)

@task
def unblock(site_alias, users):
    """Unblock the given users."""

    if type(users) is list:
        users = ','.join(users)

    return drush.run(site_alias, 'user:unblock', users)

@task
def unblock_batch(site_alias, users):
    """Unblock the given users, passed as a multline string."""

    for user in users.splitlines():
        unblock(site_alias, user)

#!/usr/bin/env python
"""Tasks for working with site databases."""

from fabric.api import hide, task

from . import drush

@task
def dump(site_alias, result_file):
    """Dump a site database at the given location."""

    return drush.run(site_alias, 'sql-dump', "--result-file=%s" % result_file)

@task
def query(site_alias, query):
    """Run an arbitrary SQL query against the given site."""

    return drush.run(site_alias, 'sql-query', query)

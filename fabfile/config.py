"""Config functions"""

import yaml

from fabric.api import hide

from . import drush

def config_delete(site_alias, config_name):
    """Delete the given config for the given site."""

    return drush.run(site_alias, 'config:delete', config_name)

def config_get(site_alias, config_name):
    """Get the given config item for the given site."""

    config = {}

    with hide('everything'):
        output = drush.run(site_alias, 'config:get', config_name)

    if output.return_code == 0 and output.strip():
        config = yaml.load(output)

    return config

def config_set(site_alias, config_name, key, value):
    """Set the given config item for the given site."""

    return drush.run(site_alias, 'config:set', config_name, key, value)
